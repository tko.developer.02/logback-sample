import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        logger.trace("[TRACE] Hello World");
        logger.debug("[DEBUG] Hello World");
        logger.info("[INFO] Hello World");
        logger.warn("[WARN] Hello World");
        logger.error("[ERROR] Hello World");

    }
}
